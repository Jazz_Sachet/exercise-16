﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            String Name;

            Console.WriteLine("*******************************");
            Console.WriteLine("****   Welcome to my app   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");
            Console.WriteLine("*******************************");
            Console.WriteLine("What is your name?");
            Name = Console.ReadLine();
            Console.WriteLine("Your Name is: "+Name );
        }
    }
}
